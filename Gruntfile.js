 module.exports = function(grunt) { //The wrapper function

 // Project configuration & task configuration
     grunt.initConfig({

          release_repo: process.env.RELEASE_REPO,

          pkg: grunt.file.readJSON('package.json'),

          //The uglify task and its configurations
          uglify: {
               options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
               },
               build: {
                 files: [{
                    expand: true,     // Enable dynamic expansion.
                    src: ['admin/js/*.js', 'public/js/hospitality*.js', 'public/js/room-reservation*.js', '!admin/js/*.min.js','!public/js/*.min.js', '!js/*.min.js'], // Actual pattern(s) to match.
                    ext: '.min.js',   // Dest filepaths will have this extension.
                 }]
               }
          },

          //The jshint task and its configurations
         jshint: {
              all: [ 'js/*.js', '!js/*.min.js' ]
         },

         watch: {

             sass: {
                 files: ['public/css/sass/*.scss', 'admin/css/sass/*.scss'],
                 tasks: ['sass', 'cssmin'],
                 options: {
                     debounceDelay: 500
                 }
             },
             readme:{
                 files: ['README.txt'],
                 tasks: ['copy:readme'],
                 options: {
                     debounceDelay: 500
                 }
             },
             js: {
                 files: ['public/js/hospitality*.js', 'public/js/room-reservation.js', 'admin/js/*.js', '!**/*.min.js'],
                 tasks: ['uglify'],
                 options: {
                     debounceDelay: 500
                 }
             }
         },

         cssmin: {
            target: {
              files: [
                { expand: true,
                    cwd: 'public/css',
                    src: ['hospitality*.css', '!*.min.css'],
                    dest: 'public/css',
                    ext: '.min.css'
                    },
                { expand: true,
                    cwd: 'admin/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'admin/css',
                    ext: '.min.css'
                    }
                ]
            },
        },

        sass:   {
            all: {
                files: {
                 'public/css/hospitality-public.css': 'public/css/sass/hospitality-public.scss',
                 'admin/css/hospitality-admin.css': 'admin/css/sass/hospitality-admin.scss'
                 }
             }
         },

         copy: {
             vendor: {
                 files: [
                     {
                         expand: true,
                         cwd: 'node_modules/bootstrap/dist/css',
                         src: 'bootstrap*',
                         dest: 'public/css'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/bootstrap/dist/fonts',
                         src: 'glyphicons-halflings-regular.*',
                         dest: 'public/fonts'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/bootstrap/dist/js',
                         src: 'bootstrap.*',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/angular-animate',
                         src: 'angular-animate.*',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/angular-sanatize',
                         src: 'angular-sanatize.*',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/angular-spinner',
                         src: 'angular-spinner.*',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/angular-touch',
                         src: 'angular-touch.*',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/angular-ui-bootstrap/dist',
                         src: 'ui-bootstrap*.js',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/angular-ui-bootstrap/dist',
                         src: 'ui-bootstrap-csp.css',
                         dest: 'public/css'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/moment/min',
                         src: 'moment-with-locals.*',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/ng-datepicker/src/css',
                         src: 'ngDatepicker.css',
                         dest: 'public/css'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/ng-datepicker/src/js',
                         src: 'ngDatepicker*.js',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/ngmap/build/scripts',
                         src: 'ng-map*.js',
                         dest: 'public/js'
                     },
                     {
                         expand: true,
                         cwd: 'node_modules/checklist-model',
                         src: 'checklist-model.js',
                         dest: 'public/js'
                     }
                ]
             },

             stage_release: {
                 files: [{
                     expand: true,
                     cwd: '.',
                     src: ['**', '!node_modules/**', '!grunt-init/**', '!Gruntfile.js', '!package.json'],
                     dest: '<%= release_repo %>'
                 }]

             },

             readme: {
                     src: 'README.txt',
                     dest: 'README.md'
             }

         }




    });

        //Loading the plug-ins
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-diff-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');




     // Default task(s), executed when you run 'grunt'
    // grunt.registerTask('default', ['uglify','cssmin']);

    grunt.registerTask('default', ['watch']);

    //Creating a custom task
    grunt.registerTask('test', [ 'jshint' ] );

};
